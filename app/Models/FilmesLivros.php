<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Reliese\Coders\Model\Relations\HasMany;

/**
 * @property-read FilmesLivrosImagens[] $imagens
 */
class FilmesLivros extends Model
{
    use HasFactory;

    protected $table = 'filmes_livros';

    protected $fillable = [
      'tipo',
      'titulo',
      'sinopse',
      'url_thumbnail',
      'url_video',
      'categoria',
    ];
    public function imagens()
    {
        return $this->hasMany(FilmesLivrosImagens::class, 'filme_livro_id');
    }
}
