<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FilmesLivrosImagens extends Model
{
    use HasFactory;

    protected $table = 'filmes_livros_images';

    protected $fillable = [
        'url'
    ];
}
