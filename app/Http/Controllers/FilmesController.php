<?php

namespace App\Http\Controllers;

use App\Models\FilmesLivros;
use Illuminate\Http\Request;

class FilmesController extends Controller
{
    public function index(Request $request){
        $values = [];

        $request->limit ??= 20;

        foreach (FilmesLivros::where('tipo', 1)->limit($request->limit)->get() as $key => $registro) {

            $values[] = [
                'id'         => $registro->id,
                'tipo'       => $registro->tipo,
                'tipo_label' => $registro->tipo == 1 ? 'Filme' : 'Livro',
                'titulo'     => $registro->titulo,
                'sinopse'    => $registro->sinopse,
                'url_thumbnail' => $registro->url_thumbnail,
                'url_video'     => $registro->url_video,
                'qtd_favoritos' => rand(10, 59),
                'qtd_valor' => rand(10, 59),
                'categoria' => $registro->categoria,
                'imagens' => $registro->imagens
            ];
        }

        return response()->json($values);
    }

    public function porId(Request $request, FilmesLivros $filme){

        $return = [
            'id'         => $filme->id,
            'tipo'       => $filme->tipo,
            'tipo_label' => $filme->tipo == 1 ? 'Filme' : 'Livro',
            'titulo'     => $filme->titulo,
            'sinopse'    => $filme->sinopse,
            'url_thumbnail' => $filme->url_thumbnail,
            'url_video'     => $filme->url_video,
            'qtd_favoritos' => rand(10, 59),
            'qtd_valor' => rand(10, 59),
            'categoria' => $filme->categoria,
            'imagens' => $filme->imagens
        ];
        return response()->json($return);
    }
}
