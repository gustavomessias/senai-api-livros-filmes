<?php

namespace App\Http\Controllers;

use App\Models\FilmesLivros;
use App\Models\FilmesLivrosImagens;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(Request $request){
        $values = [];

        foreach (FilmesLivros::limit(5)->get() as $key => $registro) {

            $values[] = [
                'id'         => $registro->id,
                'tipo'       => $registro->tipo,
                'tipo_label' => $registro->tipo == 1 ? 'Filme' : 'Livro',
                'titulo'     => $registro->titulo,
                'sinopse'    => $registro->sinopse,
                'url_thumbnail' => $registro->url_thumbnail,
                'url_video'     => $registro->url_video,
                'qtd_favoritos' => rand(10, 59),
                'qtd_valor' => rand(10, 59),
                'categoria' => $registro->categoria,
                'imagens' => $registro->imagens
            ];
        }

        return response()->json($values);
    }
}
