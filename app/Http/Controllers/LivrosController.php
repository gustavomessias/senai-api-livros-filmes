<?php

namespace App\Http\Controllers;

use App\Models\FilmesLivros;
use Illuminate\Http\Request;

class LivrosController extends Controller
{
    public function index(Request $request){
        $values = [];

        $request->limit ??= 20;

        foreach (FilmesLivros::where('tipo', 2)->limit($request->limit)->get() as $key => $registro) {

            $values[] = [
                'id'         => $registro->id,
                'tipo'       => $registro->tipo,
                'tipo_label' => $registro->tipo == 1 ? 'Filme' : 'Livro',
                'titulo'     => $registro->titulo,
                'sinopse'    => $registro->sinopse,
                'url_thumbnail' => $registro->url_thumbnail,
                'url_video'     => $registro->url_video,
                'qtd_favoritos' => rand(10, 59),
                'qtd_valor' => rand(10, 59),
                'categoria' => $registro->categoria,
                'imagens' => $registro->imagens
            ];
        }

        return response()->json($values);
    }
    public function porId(Request $request, FilmesLivros $livro){

        $return = [
            'id'         => $livro->id,
            'tipo'       => $livro->tipo,
            'tipo_label' => $livro->tipo == 1 ? 'Filme' : 'Livro',
            'titulo'     => $livro->titulo,
            'sinopse'    => $livro->sinopse,
            'url_thumbnail' => $livro->url_thumbnail,
            'url_video'     => $livro->url_video,
            'qtd_favoritos' => rand(10, 59),
            'qtd_valor' => rand(10, 59),
            'categoria' => $livro->categoria,
            'imagens' => $livro->imagens
        ];
        return response()->json($return);
    }


}
