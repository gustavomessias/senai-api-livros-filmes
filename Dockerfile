FROM php:8.2.6-fpm

# Arguments defined in docker-compose.yml
ARG user=hub
ARG uid=1000

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libgd-dev \
    jpegoptim optipng pngquant gifsicle \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libzip-dev \
    libmagickwand-dev  \
    ghostscript \
    && pecl install imagick \

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd sockets zip soap
RUN docker-php-ext-enable imagick

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Install redis
RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis

# Install xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

COPY --chown=www-data:www-data . /var/www
RUN chown -R www-data:www-data /var/www
RUN chown -R www-data:www-data /var/www/public/
RUN mkdir /srv/mysql
RUN touch /var/www/storage/logs/laravel.log
RUN chmod -R 777 /srv/mysql
RUN chmod -R 777 /var/www/storage
RUN chmod -R 755 /var/www/storage
RUN chmod -R 755 /var/www/storage/logs
RUN chmod -R 755 /var/www/storage/logs/laravel.log
RUN chmod -R 775 /var/www/storage
RUN chmod -R 775 /var/www/bootstrap/cache
RUN chmod -R 775 /var/www/public

# Set working directory
WORKDIR /var/www

USER $user
