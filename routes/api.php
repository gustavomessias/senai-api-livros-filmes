<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::prefix('/auth')->controller(AuthController::class)->group(function () {

    Route::post('/login', 'login')->name('login');
    Route::post('/register', 'register')->name('register');
    Route::post('/refresh', 'refresh')->name('refresh');
    Route::post('/logout', 'logout')->name('logout');

});

Route::prefix('/home')->controller(\App\Http\Controllers\HomeController::class)->group(function () {

    Route::get('/', 'home')->name('home');

});

Route::prefix('/filmes')->controller(\App\Http\Controllers\FilmesController::class)->group(function () {

    Route::get('/', 'index')->name('filmes');
    Route::get('/porid/{filme}', 'porId');

});

Route::prefix('/livros')->controller(\App\Http\Controllers\LivrosController::class)->group(function () {

    Route::get('/', 'index')->name('livros');
    Route::get('/porid/{livro}', 'porId');

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

